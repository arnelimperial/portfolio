# Arnel Imperial Internet Site
[![backend-python-django](https://img.shields.io/badge/backend-Python%2FDjango-informational)](https://www.djangoproject.com)
[![frontend-vuejs](https://img.shields.io/badge/Frontend-VueJS-brightgreen)](https://vuejs.org)
[![License: Zlib](https://img.shields.io/badge/License-Zlib-lightgrey.svg)](https://opensource.org/licenses/Zlib)

### Python Virtual Environment
```
python3 -m venv
```

### Project setup
```
pip install
```

```
yarn install
```

### Compiles and hot-reloads for development
```
python manage.py runserver
```

```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```
