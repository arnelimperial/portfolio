from django.conf import settings
from django.urls import path, include
from rest_framework.routers import DefaultRouter, SimpleRouter

from users.api.views import UserViewSet
from posts.api.views import PostViewSet

if settings.DEBUG:
    router = DefaultRouter()
else:
    router = SimpleRouter()

router.register("users", UserViewSet)
router.register("posts", PostViewSet)


app_name = "api"
urlpatterns = router.urls

