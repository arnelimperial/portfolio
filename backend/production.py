# from django.conf import settings

# from decouple import config, Csv

# Database
# ------------------------------------------------------------------------------
# if not settings.DEBUG:
#     DATABASES['default'] = config('DATABASE_URL')
#     DATABASES['default']['ATOMIC_REQUESTS'] = True  
#     DATABASES['default']['CONN_MAX_AGE'] = config('CONN_MAX_AGE', default=60, cast=int) 




# Security
# ------------------------------------------------------------------------------

# SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')

# SECURE_SSL_REDIRECT = config('SECURE_SSL_REDIRECT', default=True, cast=bool)

# SESSION_COOKIE_SECURE = True

# CSRF_COOKIE_SECURE = True

# SECURE_HSTS_SECONDS = 60

# SECURE_HSTS_INCLUDE_SUBDOMAINS = config(
#     'SECURE_HSTS_INCLUDE_SUBDOMAINS', default=True, cast=bool
# )

# SECURE_HSTS_PRELOAD = config('SECURE_HSTS_PRELOAD', default=True, cast=bool)

# SECURE_CONTENT_TYPE_NOSNIFF = config(
#     'SECURE_CONTENT_TYPE_NOSNIFF', default=True, cast=bool
# )