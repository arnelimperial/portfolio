from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import include, path
from django.http import HttpResponseRedirect
from django.views import defaults as default_views
from django.views.generic import TemplateView
from rest_framework.authtoken.views import obtain_auth_token

from core.views import core_view

urlpatterns = [
    path(settings.ADMIN_URL, admin.site.urls),
    path("", include('core.urls', namespace='core')),
    path("", view=core_view, name="main-app"),
    path("users/", include('users.urls', namespace='users')),
    path("accounts/", include("allauth.urls")),
    path("about/", TemplateView.as_view(template_name="pages/about.html"), name="about"),
    path("contact/", TemplateView.as_view(template_name="pages/contact.html"), name="contact"),
    path("posts/", include("posts.urls", namespace='posts')),

    #favicons
    path('favicon.ico', lambda x: HttpResponseRedirect(settings.STATIC_URL + 'images/favicons/48/favicon.ico')),
    path('favicon.ico', lambda x: HttpResponseRedirect(settings.STATIC_URL + 'images/favicons/96/favicon.ico')),
    path('favicon.ico', lambda x: HttpResponseRedirect(settings.STATIC_URL + 'images/favicons/128/favicon.ico')),
    path('robots.txt', TemplateView.as_view(template_name="robots.txt", content_type='text/plain')),
    #path('sitemap.xml', TemplateView.as_view(template_name="sitemap.xml", content_type='text/xml')),
    path('humans.txt', TemplateView.as_view(template_name="humans.txt", content_type='text/plain')),
]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

# API URLS
urlpatterns += [
    # API base url
    path("api/", include("backend.api_router")),
    # DRF auth token
    path("auth-token/", obtain_auth_token),
    # Login via browsable api
    path("api-auth/", include("rest_framework.urls")),
    # Login via REST
    path("api/rest-auth/", include("rest_auth.urls")),
    # Registration via REST
    path("api/rest-auth/registration/", include("rest_auth.registration.urls")),
]

if settings.DEBUG:
    # This allows the error pages to be debugged during development, just visit
    # these url in browser to see how these error pages look like.
    urlpatterns += [
        path(
            "400/",
            default_views.bad_request,
            kwargs={"exception": Exception("Bad Request!")},
        ),
        path(
            "403/",
            default_views.permission_denied,
            kwargs={"exception": Exception("Permission Denied")},
        ),
        path(
            "404/",
            default_views.page_not_found,
            kwargs={"exception": Exception("Page not Found")},
        ),
        path("500/", default_views.server_error),
    ]
    