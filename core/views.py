from django.shortcuts import render
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic.base import TemplateView
from django.shortcuts import HttpResponseRedirect, redirect
from django.views.decorators.cache import never_cache
from django.urls import reverse
from django.conf import settings

class IndexTemplateView(TemplateView):
    def get_template_names(self):
        template_name = "index.html"
        return template_name

core_view = never_cache(IndexTemplateView.as_view())


class AboutTemplateView(TemplateView):
    def get_template_names(self):
        if settings.DEBUG:
            template_name = "index-dev.html"
        else:
            template_name = "index.html"
        return template_name

about_view = never_cache(AboutTemplateView.as_view())


class ProjectsTemplateView(TemplateView):
    def get_template_names(self):
        if settings.DEBUG:
            template_name = "index-dev.html"
        else:
            template_name = "index.html"
        return template_name

projects_view = never_cache(ProjectsTemplateView.as_view())
