from rest_framework import serializers

from posts.models import Post


class PostSerializer(serializers.ModelSerializer):
    author = serializers.StringRelatedField(read_only=True)
    created = serializers.DateTimeField(format="%B %d, %Y", read_only=True)
    

    class Meta:
        model = Post
        fields = [
            'author',
            'headline',
            # 'subheadline',
            'slug',
            'description',
            # 'prerequisite',
            'content',
            'content1',
            'conclusion',
            'created',
            'url'
        ]
       

        extra_kwargs = {
            "url": {"view_name": "api:post-detail", "lookup_field": "slug"}
        }


        
       
        