from rest_framework import viewsets, generics, status
from rest_framework.generics import get_object_or_404
from rest_framework.exceptions import ValidationError
from rest_framework.views import APIView
from rest_framework.response import Response
from django.utils.translation import gettext_lazy as _
from rest_framework.permissions import IsAuthenticated
from rest_framework.mixins import ListModelMixin, RetrieveModelMixin, UpdateModelMixin

from .serializers import PostSerializer
from posts.models import Post
from users.api.permissions import ReadOnly, IsAdminOrReadOnly, IsAuthorOrReadOnly


class PostViewSet(viewsets.ModelViewSet):
    serializer_class = PostSerializer
    permission_classes = [IsAdminOrReadOnly]
    queryset = Post.objects.all()
    lookup_field = 'slug'

    def perform_create(self, serializer):
        serializer.save(author=self.request.user)


