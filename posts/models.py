from django.db import models
from django.conf import settings
from django.urls import reverse
from django.utils.text import slugify
from markdown_deux import markdown
User = getattr(settings, 'AUTH_USER_MODEL')


class Post(models.Model):
    author = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        related_name='posts',
        related_query_name='post'
    )
    headline = models.CharField(max_length=240)
    # subheadline = models.CharField(max_length=240, blank=True)
    slug = models.SlugField(max_length=255, unique=True, editable=False)
    description = models.CharField(max_length=240, blank=True)
    # prerequisite = models.TextField(blank=True)
    content = models.TextField()
    content1 = models.TextField(blank=True)
    conclusion = models.TextField(blank=True)
    created = models.DateTimeField(auto_now=False, auto_now_add=True)
    updated = models.DateTimeField(auto_now=True, auto_now_add=False)

    class Meta:
        ordering = ['-updated']
        unique_together = ('headline', 'slug')

    def __str__(self):
        return self.headline

    
    def get_absolute_url(self):
        return reverse('posts:detail', kwargs={'slug': self.slug})

    def save(self, *args, **kwargs):
        value = self.headline
        self.slug = slugify(value, allow_unicode=True)
        super().save(*args, **kwargs)

    def get_markdown(self):
        content = self.content
        return markdown(content)
    