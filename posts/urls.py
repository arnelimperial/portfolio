from posts.views import (
    post_list_view,
    post_detail_view,
    post_create_view,
    post_update_view,
    post_delete_view
)
from django.urls import path, include
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

app_name = 'posts'
urlpatterns = [
    path('', view=post_list_view, name='list'),
    path('<slug:slug>', view=post_detail_view, name='detail'),
    path('create/', view=post_create_view, name='create'),
    path('<slug:slug>/update/', view=post_update_view, name='update'),
    path('<slug:slug>/delete/', view=post_delete_view, name='delete'),
]