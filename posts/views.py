from django.shortcuts import render
from .models import Post
from django.views.generic import TemplateView, ListView, DetailView, CreateView, UpdateView, DeleteView
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.urls import reverse_lazy
from django.shortcuts import get_object_or_404


class PostListView(ListView):
    model = Post
    template_name = 'posts/post_list.html'
    ordering = ['-updated']
    posts = Post.objects.all()
    context_object_name = 'posts'

    def get_context_data(self, *args, **kwargs):
        context = super(PostListView, self).get_context_data(*args, **kwargs)
        context['headline'] = 'New Post'
        context['description'] = 'Awesome new post'
        return context

post_list_view = PostListView.as_view()

class PostDetailView(DetailView):
    model = Post
    template_name = 'posts/post_detail.html'
    

post_detail_view = PostDetailView.as_view()

class PostCreateView(LoginRequiredMixin,CreateView):
    model = Post
    template_name = 'posts/post_form.html'
    fields = ['headline', 'description', 'content', 'content1', 'conclusion',]

    def form_valid(self, form):
        form.instance.user = self.request.user
        return super().form_valid(form)

post_create_view = PostCreateView.as_view()

class PostUpdateView(LoginRequiredMixin, UserPassesTestMixin, UpdateView):
    model = Post
    template_name = 'posts/post_form.html'
    fields = ['headline', 'description', 'content','content1', 'conclusion',]

    def form_valid(self, form):
        form.instance.user = self.request.user
        return super().form_valid(form)

    def test_func(self):
        post = self.get_object()
        if self.request.user == post.author:
            return True
        return False

post_update_view = PostUpdateView.as_view()


class PostDeleteView(LoginRequiredMixin, UserPassesTestMixin, DeleteView):
    model = Post
    template_name = 'post/post_confirm_delete.html'
    success_url = reverse_lazy('draft_list')
    
    def test_func(self):
        post = self.get_object()
        if self.request.user == post.user:
            return True
        return False
post_delete_view = PostDeleteView.as_view()