import Vue from "vue";
import App from "./App.vue";
import router from "./router";
//import VueMarkdown from 'vue-markdown'

Vue.config.productionTip = false;
//Vue.use(VueMarkdown)
//require('./assets/css/journal-bootstrap.css')
//require('./assets/css/project.css')
import "../node_modules/bootswatch/dist/journal/bootstrap.min.css";
import "../node_modules/bootstrap/dist/css/bootstrap.min.css";
import "../node_modules/w3-css";
import "../node_modules/jquery/dist/jquery.min.js";
import "../node_modules/jquery/dist/jquery.slim.min.js";
import "../node_modules/bootstrap/dist/js/bootstrap.bundle.min.js";
import "../node_modules/@popperjs/core/dist/umd/popper-base.min.js";

new Vue({
  router,
  render: h => h(App)
}).$mount("#app");
