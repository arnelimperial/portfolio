from decouple import config

def export_vars(request):
    data = {}
    data['main_user'] = config('MAIN_USER')
    return data